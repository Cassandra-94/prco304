﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class rotateBlocks : MonoBehaviour {
    public float speed = 50f;
    public float stop = 0f;
    public Transform objectRotating;
    public float timertoWait = 20;

    public Material effect;
    public Material back;

	
	// Update is called once per frame
	void Update () {
        if (speed != 0)
            objectRotating.Rotate(Vector3.up, speed * Time.deltaTime);
    }

    void OnTriggerStay(Collider other)
    {
        if (Input.GetKeyDown(KeyCode.E))
        {
            speed = 0f;
            GetComponent<Renderer>().material = new Material(effect);
        }
        if (Input.GetKeyUp(KeyCode.E))
        {
            StartCoroutine(waitAndGo());
            GetComponent<Renderer>().material = new Material(back);
        }
        
    }
    void OnTriggerExit(Collider other)
    {
        StartCoroutine(waitAndGo());
        GetComponent<Renderer>().material = new Material(back);
    }

    IEnumerator waitAndGo()
    {
        yield return new WaitForSecondsRealtime(timertoWait);
        speed = 50f;

    }
}
