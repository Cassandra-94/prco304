﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class playerTransport : MonoBehaviour {

    public Transform player;
    public Transform playerEnd;
    public float timeToTransport = 0f;

    [SerializeField]
    private float intepval = 0f;

    public Material effect;
    public Material back;

    private Vector3 playerStart;

    void OnTriggerStay(Collider other)
    {
      //  print("Entered Transport");
        playerStart = player.position;
     //   print("Player position");
       if (Input.GetKeyDown(KeyCode.E))
        {
         //   print("Key E down");
            Invoke("PlayerTransport", 0f);
            GetComponent<Renderer>().material = new Material(effect);
        }

    }

    void OnTriggerExit(Collider other)
    {
        GetComponent<Renderer>().material = new Material(back);

    }
    
    private void PlayerTransport()
    {
      //  print("Started Corotine");
        StartCoroutine(PlayerMove());
    }

    private IEnumerator PlayerMove()
    {
       // print("Ienumerator");
        float timeToFinnish = 0.0f;
        while (intepval <= 1)
        {
            timeToFinnish += Time.deltaTime;
            intepval = timeToFinnish / timeToTransport;
            player.position = Vector3.Lerp(playerStart, playerEnd.position, intepval);
               yield return null;
        }
        intepval = 0f;
    }
}
