﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class rotateoncommand : MonoBehaviour {
    public float rotateSpeed = 0f;
    public float start = 50f;
    public Transform objectRotating;

    public Material effect;
    public Material back;

    // Update is called once per frame
    void Update () {
        if (rotateSpeed !=  0)
            objectRotating.Rotate(Vector3.up, rotateSpeed * Time.deltaTime);
    }

    void OnTriggerStay(Collider other)
    {
        if (Input.GetKeyDown(KeyCode.E))
        {
            rotateSpeed = 50f;
            GetComponent<Renderer>().material = new Material(effect);
        }
        if (Input.GetKeyUp(KeyCode.E))
        {
            rotateSpeed = 0f;
            GetComponent<Renderer>().material = new Material(back);
        }
   
    }
    void OnTriggerExit(Collider other)
    {
        rotateSpeed = 0f;
        GetComponent<Renderer>().material = new Material(back);
    }
}
