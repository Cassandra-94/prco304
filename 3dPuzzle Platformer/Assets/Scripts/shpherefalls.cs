﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class shpherefalls : MonoBehaviour {


  private Vector3 start;
    public Transform movingball;
    public Transform restBall;

    public float timeTakenReset = 0.5f;

    [SerializeField]
    private float interpVal = 0.0f;
    //// Use this for initialization
    void Start()
    {
        start = movingball.position;
    }

    // Update is called once per frame
    void Update()
    {

    }


    //I am now trying a reset button instead of a trigger. this will help if the ball gets stuck.  

    //void OnTriggerEnter(Collider collider)
    //{
    //    if (collider.gameObject.tag == "Ball")
    //    {
    //        Destroy(gameObject);

    //        transform.position = start;
    //    }
    //}



    void OnTriggerEnter(Collider other)
    {
        Invoke("StartMoving", 0.0f);
        //  isXDown();



    }
    private void StartMoving()
    {
        StartCoroutine(StartCounter());
    }

    private IEnumerator StartCounter()
    {
        float timeLeftToFinish = 0.0f;
        while (interpVal <= 1)
        {
            timeLeftToFinish += Time.deltaTime;
            interpVal = timeLeftToFinish / timeTakenReset;
            movingball.position = Vector3.Lerp(start, restBall.position, interpVal);
            yield return null;
        }
        interpVal = 0f;
    }
}
