﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class Inventory : MonoBehaviour {
    public int ball;
    public int collectable;
    private float a_renerDist = 2f;
    public int squiggle;
    public int spinndle;
    public int cone;
    public int pill;
    public int pryamid;

    public Text ballInv;
    public Text spinInv;
    public Text squigInv;
    public Text coneInv;
    public Text pillInv;
    public Text pryInv;


    public float doorRotate = 0f;
    public float openDoor = 10f;
    public Transform door;

    // Use this for initialization
    void Start () {
        ball = 0;
        squiggle = 0;
        collectable = 0;
        spinndle = 0;
        cone = 0;
        pill = 0;
        pryamid = 0;

      
    }
	
	// Update is called once per frame
	void Update () {
        Ray camRay = Camera.main.ViewportPointToRay(new Vector3(0.5f, 0.5f, 0.0f));
        RaycastHit aHit;

        if (Physics.Raycast(camRay, out aHit, a_renerDist))
        {
            Debug.DrawRay(camRay.origin, camRay.direction * aHit.distance, Color.blue);

            if (Input.GetButtonDown("Collect"))
            {
                if (aHit.collider.tag == "Inventory")
                {
                    Destroy(aHit.collider.gameObject);
                    collectable++;
                }

            }

            if (Input.GetMouseButtonDown(0))
            {
                if (aHit.collider.tag == "Ball")
                {
                    Destroy(aHit.collider.gameObject);
                    ball++;
                    collectable++;
                    
                }

                else if (aHit.collider.tag == "Squiggle")
                {
                    Destroy(aHit.collider.gameObject);
                    squiggle++;
                    collectable++;

                }

                else if (aHit.collider.tag == "Spinndle")
                {
                    Destroy(aHit.collider.gameObject);
                    spinndle++;
                    collectable++;

                }

                else if (aHit.collider.tag == "Cone")
                {
                    Destroy(aHit.collider.gameObject);
                    cone++;
                    collectable++;
                }

                else if (aHit.collider.tag == "Pill")
                {
                    Destroy(aHit.collider.gameObject);
                    pill++;
                    collectable++;
                }

                else if (aHit.collider.tag == "Pryamid")
                {
                    Destroy(aHit.collider.gameObject);
                    pryamid++;
                    collectable++;
                }

               
            }

            
            
        
        }
        ballInv.text = "Ball: " + ball.ToString();
        spinInv.text = "Spinndle: " + spinndle.ToString();
        squigInv.text = "Squiggle: " + squiggle.ToString();
        coneInv.text = "Cone: " + cone.ToString();
        pillInv.text = "Pill: " + pill.ToString();
        pryInv.text = "Pryamid: " + pryamid.ToString();

        if (doorRotate != 0)
            door.Rotate(Vector3.up, doorRotate * Time.deltaTime);

    }


    void OnTriggerStay(Collider other)
    {
        //temp colectable. change / more doors chalenges with actual item names
        if (collectable == 6)
        {
            // temp rotate look to anmation or something
            doorRotate = openDoor;
        }
        else
        {
            doorRotate = 0;
        }

    }
    void OnTriggerExit(Collider other)
    {
        doorRotate = 0;
    }
}
