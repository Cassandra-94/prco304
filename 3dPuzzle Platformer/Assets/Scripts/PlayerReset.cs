﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerReset : MonoBehaviour
{


    public Transform player;
    public Transform playerEnd;
    public float timeToTransport = 0f;

    [SerializeField]
    private float intepval = 0f;

    private Vector3 playerStart;
    private Vector3 tempPlayerPosition;

    // Use this for initialization
    void Start()
    {


    }

    // Update is called once per frame
    void Update()
    {

        // tempPlayerPosition = playerStart;

    }

    void OnTriggerStay(Collider other)
    {
        //  print("Entered Transport");
        playerStart = player.position;
        //   print("Player position");

        

        if (Input.GetKeyDown(KeyCode.P))
        {
          
            //   print("Key E down");
        Invoke("PlayerTransport", 0f);
            
        }

     



    }






    private void PlayerTransport()
    {
        //  print("Started Corotine");
        StartCoroutine(PlayerMove());
    }

    private IEnumerator PlayerMove()
    {
        // print("Ienumerator");
        float timeToFinnish = 0.0f;
        while (intepval <= 1)
        {
            timeToFinnish += Time.deltaTime;
            intepval = timeToFinnish / timeToTransport;
            player.position = Vector3.Lerp(playerStart, playerEnd.position, intepval);
            yield return null;
        }
        intepval = 0f;
    }
}
