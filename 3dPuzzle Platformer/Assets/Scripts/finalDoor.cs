﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class finalDoor : MonoBehaviour {

    public float rotateSpeed = 0f;
    public Transform door;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        if (rotateSpeed != 0)
            door.Rotate(Vector3.up, rotateSpeed * Time.deltaTime);
    }

    void OnTriggerStay(Collider other)
    {
       
    }

    void OnTriggerExit(Collider other)
    {
        rotateSpeed = 0f;
    }
}
