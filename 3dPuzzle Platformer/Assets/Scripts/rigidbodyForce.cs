﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class rigidbodyForce : MonoBehaviour {

    public float thrust;
    //public float torque;
    public Rigidbody rb;

	// Use this for initialization
	void Start () {
        rb = GetComponent<Rigidbody>();
	}
	
	// Update is called once per frame
	void FixedUpdate () {

        //rb.AddForce(thrust, thrust, thrust , ForceMode.Impulse);

        //current version
         rb.AddRelativeForce(Vector3.forward * thrust);

        //torque
        // float turn = Input.GetAxis("Horizontal");
        //  rb.AddTorque(transform.up * torque * turn);

        ApplyForce(rb);
	}

    void ApplyForce(Rigidbody body)
    {
        Vector3 direction = body.transform.position - transform.position;
        body.AddForceAtPosition(direction.normalized, transform.position);
    }

 
}
