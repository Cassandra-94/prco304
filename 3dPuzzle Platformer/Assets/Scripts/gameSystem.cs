﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;


public class gameSystem : MonoBehaviour {
  

    // Use this for initialization
    void Start () {
        
        Cursor.visible = true;
        Cursor.lockState = CursorLockMode.None;

    }
	
	// Update is called once per frame
	void Update () {
        Cursor.visible = true;
        Cursor.lockState = CursorLockMode.None;
    }

    public void LoadLevel(int levelIndex)
    {
       // Application.LoadLevel(levelIndex);
        SceneManager.LoadScene(levelIndex);
        
    }

    public void QuitLevel()
    {
        Application.Quit();
    }
}
