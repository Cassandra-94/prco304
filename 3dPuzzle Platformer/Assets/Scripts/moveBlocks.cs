﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class moveBlocks : MonoBehaviour {

    public Transform objectThatWillMove;
    public Transform whereTheObjectWillMove;
    public float timeTakenMoving = 1.0f;
  

    [SerializeField]
    private float interpVal = 0.0f;
    private Vector3 startingPlace;

    public Material effect;
    public Material back;

    // Use this for initialization
    void Start () {
       // a_transform = transform;
        startingPlace = objectThatWillMove.position;
	}




    void OnTriggerStay(Collider other)
    {
        //print("stay");
        if (Input.GetKeyDown(KeyCode.E))
        {
            Invoke("StartMoving", 0.0f);
            GetComponent<Renderer>().material = new Material(effect);
        }
        if(Input.GetKeyUp(KeyCode.E))
        {
            Invoke("StopMoving", 0.0f);
            GetComponent<Renderer>().material = new Material(back);
        }

        
    }

    private void StartMoving()
    {
        StartCoroutine(StartCounter());
    }
    private void StopMoving()
    {
        StartCoroutine(EndCounter());
    }
    //this code shows how it can be activated. (should do)
    //void OnTriggerEnter(Collider other)
    //{

    //    Invoke("StartMoving", 0.0f);
    //}

    //void OnTriggerExit(Collider other)
    //{
    //    //print("i exists!");
    //    Invoke("StopMoving", 0.0f);

    //}

    // Update is called once per frame

    //void OnTriggerEnter(Collider other)
    //{

    //        print("enter");
    //    Invoke("StartMoving", 0.0f);
       
    //    //  isXDown();
    //    if (Input.GetButtonDown("Activate"))
    //    {
         

    //        print("x is down");
    //    }
      
        

    //}
    void OnTriggerExit(Collider other)
    {
       
           Invoke("StopMoving", 0.0f);
        GetComponent<Renderer>().material = new Material(back);

        // print("exit");
    }
    

    //void isXDown()
    //{
    //    if (Input.GetButtonDown("Activate"))
    //    {
    //        Invoke("StartMoving", 0.0f);
    //    }
    //    else if (Input.GetButtonUp("Activate"))
    //    {
    //        Invoke("StopMoving", 0.0f);
    //    }
    //}


    void Update () {
   

            //if (Input.GetButtonDown("Activate"))
            //{
            //    Invoke("StartMoving", 0.0f);
            //}
            //else if (Input.GetButtonUp("Activate"))
            //{
            //    Invoke("StopMoving", 0.0f);
            //}

        }

    private IEnumerator StartCounter()
    {
        float timeLeftToFinish = 0.0f;
        while (interpVal <=1)
        {
            timeLeftToFinish += Time.deltaTime;
            interpVal = timeLeftToFinish / timeTakenMoving;
            objectThatWillMove.position = Vector3.Lerp(startingPlace, whereTheObjectWillMove.position, interpVal);
            yield return null;
        }
        interpVal = 0f;
    }

    private IEnumerator EndCounter()
    {
        float timeLeftToFinish = 0.0f;
        while (interpVal <=1)
        {
            timeLeftToFinish += Time.deltaTime;
            interpVal = timeLeftToFinish / timeTakenMoving;
            objectThatWillMove.position = Vector3.Lerp(objectThatWillMove.position, startingPlace, interpVal);
            yield return null;
        }
        interpVal = 0f;
    }
}
